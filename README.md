# Flask & Prometheus

Simple Monitoring Example with Flask App.

## Running Flask app

~~~~
docker build -t flask-app:0.1.0 .
docker run -d -p 5000:5000 --name flask-app flask-app:0.1.0
~~~~

## Running Prometheus

~~~~
docker run -d \
    -p 9090:9090 \
    -v $PWD/config/prometheus/prometheus.yml:/etc/prometheus/prometheus.yml \
    --link flask-app \
    --name prometheus \
    prom/prometheus
~~~~

## Check it's working

Example endpoint:

``http://localhost:5000/test``

Example query:

``flask_http_request_total{job="flask-app"}``

## Loki (Optional)

~~~~
docker run -d --name=loki -p 3100:3100 \
-v $PWD/config/loki/config-loki.yml:/usr/local/bin/config-loki.yml \
grafana/loki
~~~~

## Promtail (Optional)

The job on the config/promtail/config-promtail.yml is monitoring journals.

~~~~
docker run -d --name promtail -p 9080 \
-v $PWD/config/promtail/config-promtail.yml:/etc/promtail/config.yml \
-v $PWD/logs/nginx.log:/var/log/nginx/nginx.log \
--link loki \
grafana/promtail
~~~~

Query examples on the explore interface on Grafana:

~~~~
{job=”nginx”}
{job=”nginx”} Info
{job=”nginx”} Error
{job="nginx"} Info|Error
#Or For A Case Insensitive Search
{job=”nginx”} (?i)error
~~~~

## Running Grafana (Optional)

~~~~
docker run -d \
-p 3000:3000 \
-v  $PWD/config/grafana/automatic.yml:/etc/grafana/provisioning/datasources/automatic.yml \
--name=grafana \
--link loki \
--link prometheus \
grafana/grafana
~~~~

Default credentials: admin / admin

# References

## Prometheus

https://prometheus.io/docs/prometheus/latest/querying/basics/

https://github.com/rycus86/prometheus_flask_exporter/blob/master/examples/restplus-default-metrics/server.py

https://github.com/rycus86/prometheus_flask_exporter/blob/master/README.md

https://blog.viktoradam.net/2020/05/11/prometheus-flask-exporter/

## Grafana

https://grafana.com/docs/grafana/latest/installation/docker/

https://community.grafana.com/t/data-source-on-startup/8618/2

https://grafana.com/docs/grafana/latest/administration/provisioning/#datasources?utm_source=grafana_ds_list

## Loki & Promtail

https://medium.com/grafana-tutorials/setup-grafana-logs-panel-with-loki-and-promtail-3bd89cf40c31

https://rtfm.co.ua/en/grafana-labs-loki-logs-collector-and-monitoring-system/

https://grafana.com/docs/loki/latest/clients/promtail/configuration/

## Huge Nginx Logs

https://raw.githubusercontent.com/elastic/examples/master/Common%20Data%20Formats/nginx_logs/nginx_logs